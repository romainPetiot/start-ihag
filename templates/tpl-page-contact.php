<?php
/**
 * Template Name: post archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<div id="page-<?php the_ID(); ?>">

		<?php
        the_content();
		?>
        <form name="whitepaperForm" id="whitepaperForm" action="#" method="POST">
            <input type="hidden" name="honeyPotWhitePaper" value="">
            <div class="formCont">
                <label for="namewhitepaper">Nom</label>
                <input type="text" name="namewhitepaper" id="namewhitepaper" placeholder="Dupont" required value="">
            </div>
            <div class="formCont">
                <label for="firstnamewhitepaper">Pr&eacute;nom</label>
                <input type="text" name="firstnamewhitepaper" id="firstnamewhitepaper" placeholder="Jean-Pierre" required value="">
            </div>
            
            <div class="formCont">
                <label for="emailwhitepaper">Adresse mail</label>
                <input type="email" name="emailwhitepaper" id="emailwhitepaper" placeholder="nom.prenom@exemple.com" required value="">
            </div>
            
            <div class="formCont">
                <label for="phonewhitepaper">Téléphone</label>
                <input type="tel" name="" id="phonewhitepaper" placeholder="Téléphone" required >
            </div>

            <div class="formCont">
                <label for="companywhitepaper">Société</label>
                <input type="text" name="companywhitepaper" id="companywhitepaper" placeholder="Société" required value="">
            </div>

            <div class="formCont">
                <label for="activitywhitepaper">Secteur d'activité</label>
                <input type="text" name="activitywhitepaper" id="activitywhitepaper" placeholder="Secteur d'activité" required value="">
            </div>

            <div class="formCont checkbox">
                <input type="checkbox" name="NewsletterCheckbox">
                <label for="NewsletterCheckbox">Je souhaite recevoir les newsletter de Digitemis</label>
            </div>

            <div class="formCont checkbox">
                <input type="checkbox" name="recontactCheckbox">
                <label for="recontactCheckbox">Je ne souhaite pas être recontacté par Digitemis</label>
            </div>


            <div id="ResponseAnchor">
                <input class="cta-standard" type="submit" id="sendMessage" value="Envoyer">
            <div id="ResponseMessage">
                <?php _e("Votre message a été envoyé !", 'IHAG');?>
            </div>
        </form>
</div><!-- #page-<?php the_ID(); ?> -->

<?php endwhile; endif; ?>

<?php get_footer(); ?>