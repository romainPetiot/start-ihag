<?php
/**
 * Template Name: advertising archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>





    <div class="">         
            <?php
            global $post;
            $args = array( 
                    'posts_per_page'   => -1,
                    'post_type'        => 'advertising',
                    'post_status'      => 'publish'
                );
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : 
            setup_postdata( $post ); 
            ?>
                <div class="">
                    <?php the_post_thumbnail("600-600");?>
                    <h2><?php the_title();?></h2>
                    <?php the_field("content");?>
                    <a href="<?php the_field("file"); ?>" download>
                        <?php _e("Télécharger", "digitemis");?>
                    </a>
                </div>
            <?php endforeach;
            wp_reset_postdata(); ?>
    </div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
