<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

get_header();
?>

	<div id="primary">
		<main id="main" class="wrapper">

		<?php if ( have_posts() ) : ?>
			<?php
				$terms = get_terms( array(
					'taxonomy' => 'category',
				) );
				?>
				<ul>
				<?php foreach ( $terms as $term ) :?>
					<li class="js-change-category" data-categoy="<?php echo $term->term_id;?>">
						<a href="<?php echo get_term_link($term);?>">
							<?php echo '<h2>'.$term->name . '</h2>';?>
						</a>
					</li>
				<?php endforeach;?>
				</ul>
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_type() );
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
