<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'monprojet',
                'title' => __( 'monprojet', 'ihag' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {
        

        acf_register_block_type(
            array(
                'name'				    => 'last-posts',
                'title'				    => __('Derniers articles'),
                'description'		    => __('Derniers articles'),
                'placeholder'		    => __('Derniers articles'),
                'render_template'	    => 'template-parts/block/last-posts.php',
                'category'			    => 'monprojet',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('actualité', 'derniers', 'articles', 'post'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            'mode' => false,
                                        ),
            )
        );
        
    }
}


<?php
// Ajoute une feuille de styles  dans l'admin
//add_theme_support('editor-styles');
//add_editor_style(get_stylesheet_directory_uri().'/assets/css/admin-editor.css');
function my_theme_setup() {
  	
  // Nouveauté à ajouter
  add_theme_support('editor-styles');

  // Puis la même fonction qu'on utilisait auparavant pour Tiny MCE
  add_editor_style( 'style-editor.css' );
  //add_editor_style(get_stylesheet_directory_uri().'/assets/css/admin-editor.css');
  //add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );

// FILTRES LES BLOCS AUTORISES SUR LE SITE
// cf. https://rudrastyh.com/gutenberg/remove-default-blocks.html
// -----------------------------------------------------------------------------
add_filter( 'allowed_block_types', 'nb_allowed_block_types', 10, 2 );
function nb_allowed_block_types($allowed_blocks, $post) {
	$allowed_blocks = array(

	// Blocs spécifiques du theme 
    'acf/last-posts',
    
    // Blocs communs
        'core/heading',
		'core/paragraph',
  	//'core/image',
  	//'core/gallery',
		'core/list',
		//'core/quote',
		// 'core/audio',
		//'core/cover-image',
		//'core/file',
		//'core/video',
		//'core/media',

    // Mise en forme
    //'core/table',
    // 'core/verse',
    // 'core/code',
    // 'core/freeform',
    // 'core/html',
    // 'core/preformatted',
    // 'core/pullquote',

    // Mise en page
    'core/button',
    //'core/columns',
    //'core/media-text',
    // 'core/more',
    // 'core/nextpage',
    // 'core/separator',
    // 'core/spacer',

    // Widgets
    // 'core/shortcode',
    // 'core/archives',
    // 'core/categories',
    // 'core/latest-comments',
    // 'core/latest-posts',

    // Contenus embarqués
    // 'core/embed',
    // 'core-embed/youtube',
    // 'core-embed/facebook',
    // 'core-embed/twitter',
    // 'core-embed/instagram',
    // core-embed/wordpress
    // core-embed/soundcloud
    // core-embed/spotify
    // core-embed/flickr
    // core-embed/vimeo
    // core-embed/animoto
    // core-embed/cloudup
    // core-embed/collegehumor
    // core-embed/dailymotion
    // core-embed/funnyordie
    // core-embed/hulu
    // core-embed/imgur
    // core-embed/issuu
    // core-embed/kickstarter
    // core-embed/meetup-com
    // core-embed/mixcloud
    // core-embed/photobucket
    // core-embed/polldaddy
    // core-embed/reddit
    // core-embed/reverbnation
    // core-embed/screencast
    // core-embed/scribd
    // core-embed/slideshare
    // core-embed/smugmug
    // core-embed/speaker
    // core-embed/ted
    // core-embed/tumblr
    // core-embed/videopress
    // core-embed/wordpress-tv
  );
  
  if($post->ID == get_option( 'page_on_front' )){
    $allowed_blocks[] = 'acf/carrousel-home';
  }

	return $allowed_blocks;
}




// DESACTIVE GUTENBERG DE CERTAINS TEMPLATES DE PAGE
// -----------------------------------------------------------------------------
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// Désactive Gutenberg par template
function ea_disable_gutenberg( $can_edit, $post_type ) {

  if(is_page_template( 'templates/tpl-range-recipe.php' )){
    return true;
  }

  if($post_type == "farmer"){
    return false;
  }
  if($post_type == "recipe"){
    return false;
  }

	if( ! ( is_admin() && !empty( $_GET['post'] ) ) ){
    return $can_edit;
  }

	return $can_edit;
}



// NETTOYAGE DES OPTIONS DE LA SIDEBAR
// cf. https://joseph-dickson.com/removing-specific-gutenberg-core-blocks-and-options/
// -----------------------------------------------------------------------------


// PALETTE DE COULEURS
// -----------------------------------------------------------------------------
// Désactive la palette de couleurs
add_theme_support( 'disable-custom-colors' );

// Supprime la palette de couleur
add_theme_support( 'editor-color-palette' );


// FONTS
// -----------------------------------------------------------------------------
// Désactive les tailles de typos
add_theme_support( 'disable-custom-font-sizes' );

// Crée une liste de choix de typos adaptée à la charte du site
add_theme_support( 'editor-font-sizes',
  array(
  	array(
  		'name' => "Petit",
  		'shortName' => 'S',
  		'size' => 12,
  		'slug' => 'small'
  	),
  	array(
    	'name' => 'Standard',
    	'shortName' => 'D',
    	'size' => 16,
    	'slug' => 'standard'
  	),
  	array(
    	'name' => 'Moyen',
    	'shortName' => 'M',
    	'size' => 20,
    	'slug' => 'medium'
  	),
  	array(
  		'name' => 'Grand',
  		'shortName' => 'L',
  		'size' => 24,
  		'slug' => 'large'
  	),
  	array(
  		'name' => 'Très grand',
    	'shortName' => 'XL',
  		'size' => 28,
  		'slug' => 'extra-large'
  	)
  )
);
