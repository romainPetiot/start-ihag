
document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("link-menu").addEventListener("click", function(e) {
        e.preventDefault();
        document.getElementById("menu-mobile-container").classList.toggle("openMenu");
        document.getElementById("link-menu").classList.toggle("close-menu");
    });
    //document.getElementById("menu").addEventListener("click", function(e) {
    //    document.getElementById("menu").classList.remove("openMenu");
    //});
});

// topbar gets sticky on scroll
var scroll = document.getElementById("masthead");
var menu = document.getElementById("masthead");
var sticky = scroll.offsetHeight / 2;
window.onscroll = function() {
    if (window.pageYOffset > sticky) {
        menu.classList.add("sticky");
    } else {
        menu.classList.remove("sticky");
    }
};

function toggleSearchbar() {
  document.getElementById("form-search").classList.toggle("openSearchbar");
}
