<?php
/**
* Block Name: Bloc Chapo
*/
?>
<section class="chapo bloc-vertical-spacing">
<?php
$chapo = get_field('title');
if ( !$chapo ) :
	?>
	<div style="text-align:center">
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper center">
		<h1>
			<?php the_field('title');?><br>
			<span class="font-cursive regular"><?php the_field('sub-title');?></span>
		</h1>
		<?php the_field('content');?>
	</div>
	<?php
endif;
?>
</section>
