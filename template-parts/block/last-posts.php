<?php
/**
* Block Name: Bloc last-posts
*/
?>
<section class="last-posts">
	<div class="wrapper bloc-vertical-spacing">
		<h2 class="center underline"><?php _e("Derniers Articles", 'digitemis');?></h2>
		<?php
		global $post;
			$lastposts = get_posts( array(
				'posts_per_page' => 2,
				'post_status'    => 'publish'
			) );
		?>
		<div class="post-container">
			<?php
				if ( $lastposts ) {
					foreach ( $lastposts as $post ) :
						
						setup_postdata( $post ); 
						$term_obj_list = get_the_terms($post, 'category');
						$cat = join(', ', wp_list_pluck($term_obj_list, 'name'));
						?>
						<a class="single-post" href="<?php the_permalink();?>" title="<?php the_title();?>">
							<div>
								<?php the_post_thumbnail("thumb-post");?>
								<p class="category">
									<?php echo $cat;?>
								</p>
								<h3><?php the_title(); ?></h3>
								<time datetime="<?php echo get_the_date('c');?>2012-02-11"><?php echo get_the_date();?></time>
								<div class="post-excerpt"><?php the_excerpt();?></div>
							</div>
						</a>
					<?php
					endforeach; 
					wp_reset_postdata();
				}
			?>
		</div><!-- .post-container -->
	</div><!-- .wrapper -->
</section>
