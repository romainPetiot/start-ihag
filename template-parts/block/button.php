<?php
/**
* Block Name: Bloc button
*/
?>
<section class="button-bloc bloc-vertical-spacing ">
<?php
if(!get_field('label-button') || !get_field('link-button')):?>
	?>
	<div style="text-align:center">
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
		<a href="<?php echo get_field('link-button');?>" class="button uppercase">
			<?php echo get_field('label-button');?>
		</a>
	<?php
endif;
?>
</section>
